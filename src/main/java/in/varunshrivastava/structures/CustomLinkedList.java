package in.varunshrivastava.structures;

import in.varunshrivastava.contracts.List;

public class CustomLinkedList<E> implements List<E> {

	private Node<E> first;
	private Node<E> last;
	private int size;
	
	public boolean add(E data) {
		return linkNode(data);
	}

	public boolean remove(E data) {
		return removeLinkToNode(getNode(data));
	}


	public int size() {
		return size;
	}

	private Node<E> getNode(int index) {
		Node<E> currNode = first;
		int counter = 0;
		while (isNodeNull(currNode)) 
		{
			if (isRequiredIndex(index, counter)) 
			{
				return currNode;
			}
			counter++;
			currNode = currNode.next;
			
		}
		return null;
	}
	
	

	private Node<E> getNode(E data) {
		Node<E> currNode = first;
		while (isNodeNull(currNode)) 
		{
			if (currNode.data == data) 
			{
				return currNode;
			}
			currNode = currNode.next;
		}
		return null;
	}
	
	public boolean remove(int index) {
		return unlinkNode(index);
	}

	public E get(int index) {
		return getNode(index).data;
	}

	public E get(E item) {
		return getNode(item).data;
	}
	
	private static class Node<E> {
		Node<E> prev;
		E data;
		Node<E> next;
		
		Node(Node<E> prev, E data, Node<E> next) {
			this.prev = prev;
			this.data = data;
			this.next = next;
		}
	}

	
	private boolean linkNode(E data) {
		Node<E> newNode = new Node<E>(last, data, null);
		if (isListEmpty()) 
		{
			return linkFirstNode(newNode);
		}
		return addAtLast(newNode);
	}

	private boolean addAtLast(Node<E> newNode) {
		Node<E> secondLast = last;
		last = newNode;
		secondLast.next = last;
		last.next = null;
		size++;
		return true;
	}

	private boolean linkFirstNode(Node<E> newNode) {
		last = newNode;
		first = last;
		first.prev = null;
		size++;
		return true;
	}
	
	
	private boolean unlinkNode(int index) {
		return removeLinkToNode(getNode(index));
	}

	private boolean removeLinkToNode(Node<E> currNode) {
		currNode.prev = currNode.next;
		currNode = null;
		size--;
		return true;
	}

	private boolean isRequiredIndex(int index, int counter) {
		return counter == index;
	}

	private boolean isNodeNull(Node<E> currNode) {
		return currNode != null;
	}
	
	private boolean isListEmpty() {
		return first == null;
	}
}
