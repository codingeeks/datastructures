package in.varunshrivastava.contracts;

public interface List<E> {
	public boolean add(E item);
	public boolean remove(E item);
	public boolean remove(int index);
	public int size();
	public E get(E item);
	public E get(int index);
}