package in.varunshrivastava.structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CustomLinkedListTest {

	CustomLinkedList<Double> doubleList;
	
	@Before
	public void setUp() 
	{
		doubleList = new CustomLinkedList();
	}
	
	@Test
	public void itShouldReturnAddOneItem() 
	{
		boolean flag = doubleList.add(34.00d);
		assertTrue(flag);
	}
	
	@Test
	public void itShouldGetItemByIndex()
	{
		Double d = new Double(22.98d);
		doubleList.add(d);
		assertEquals(d, doubleList.get(0));
	}
	
	@Test
	public void itShouldGetItemByObject() 
	{
		Double d = new Double(22.98d);
		doubleList.add(d);
		assertEquals(d, doubleList.get(d));
	}
	
	@Test
	public void itShouldReturnListSize() 
	{
		doubleList.add(34.00d);
		doubleList.add(40.55d);
		assertEquals(2, doubleList.size());
	}
	
	@Test
	public void itShouldRemoveItemFromIndex() {
		doubleList.add(34.00d);
		doubleList.add(40.55d);
		doubleList.add(50.55d);
		boolean flag = doubleList.remove(2);
		assertTrue(flag);
		assertEquals(2, doubleList.size());
	}
	
	@Test
	public void itShouldRemoveItemByObject() {
		Double d1 = new Double(34.00d);
		Double d2 = new Double(35.00d);
		doubleList.add(d1);
		doubleList.add(d2);
		boolean flag = doubleList.remove(d1);
		assertTrue(flag);
		assertEquals(1, doubleList.size());
	}
}
